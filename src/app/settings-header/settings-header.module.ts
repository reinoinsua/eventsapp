import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsHeaderComponent } from './settings-header.component';

@NgModule({
  imports: [IonicModule, CommonModule],
  declarations: [SettingsHeaderComponent],
  exports: [SettingsHeaderComponent]
})
export class SettingsHeaderModule {}
