import { Component, OnInit, Input } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Events } from '@ionic/angular';
import { User } from '../models/user';

@Component({
  selector: 'settings-header',
  templateUrl: './settings-header.component.html',
  styleUrls: ['./settings-header.component.scss']
})
export class SettingsHeaderComponent implements OnInit {
  user: User;
  userLetter: string;
  @Input() userName: string;

  constructor(
    public fAuth: AngularFireAuth,
    public events: Events
  ) {
    this.events.subscribe('user:saved', user => {
      this.user = new User(user);
      this.userLetter = this.user ? this.getUserLetter(this.user) : 'U';
      this.userName = this.user ? this.getUserName(this.user) : 'User';
    });
  }

  ngOnInit() {}

  logOut() {
    this.fAuth.auth.signOut();
  }

  getUserLetter(user) {
    if (!user) {
      this.userLetter = this.userName.charAt(0);
      return this.userLetter;
    }
    return this.getUserName(user).charAt(0);
  }

  getUserName(user) {
    return user.name || user.email;
  }
}
