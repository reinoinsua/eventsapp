import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserDataPage } from './user-data.page';
import { AppHeaderModule } from '../app-header/app-header.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: UserDataPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AppHeaderModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [UserDataPage]
})
export class UserDataPageModule {}
