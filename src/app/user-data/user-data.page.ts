import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProfileService } from '../services/profile.service';
import { AlertController } from '@ionic/angular';
import { User } from '../models/user';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.page.html',
  styleUrls: ['./user-data.page.scss']
})
export class UserDataPage implements OnInit {
  updaterForm: FormGroup;
  showFirebaseERror = false;
  firebaseMessage: string;
  user: User;
  isDone = false;
  translations: {
    update_success: '';
  };

  constructor(
    public fAuth: AngularFireAuth,
    private formBuilder: FormBuilder,
    public profileService: ProfileService,
    public alertController: AlertController,
    private route: ActivatedRoute,
    public translateService: TranslateService
  ) {
    this.route.queryParams.subscribe(params => {
      this.user = JSON.parse(params['user']);
      this.createForm();
    });
    this.loadTranslations();
  }

  ngOnInit() {}

  createForm() {
    this.updaterForm = this.formBuilder.group({
      email: [this.user.email],
      name: [this.user.name],
      locale: [this.user.locale]
    });
    this.isDone = true;
  }

  update() {
    const userId = this.fAuth.auth.currentUser.uid;
    const data = {
      name: this.updaterForm.value.name,
      locale: this.updaterForm.value.locale,
      email: this.updaterForm.value.email
    };
    this.profileService.updateUser(userId, data);
    this.presentInfo();
  }

  profileUpdated() {
    this.presentInfo();
  }

  async presentInfo() {
    const alert = await this.alertController.create({
      header: 'Info',
      message: this.translations.update_success,
      buttons: ['Ok']
    });

    await alert.present();
  }

  loadTranslations() {
    this.translateService.get('USER_DATA.update_success').subscribe(value => {
      this.translations.update_success = value;
    });
  }
}
