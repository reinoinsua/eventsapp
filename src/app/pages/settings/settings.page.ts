import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { ProfileService } from '../../services/profile.service';
import { Events, NavController, AlertController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss']
})
export class SettingsPage implements OnInit {
  user: User;

  routes = [
    { icon: 'person', text: 'SETTINGS.your_data', path: 'userData' },
    { icon: 'lock', text: 'SETTINGS.reset_password', path: 'resetPassword' }
  ];

  translations: {
    info: '',
    warning: '',
    delete_confirmation: '',
    cancel: '',
    delete_account: '',
    delete_error: '',
  };

  constructor(
    public fAuth: AngularFireAuth,
    public profileService: ProfileService,
    public events: Events,
    public navCtrl: NavController,
    public alertController: AlertController,
    public storageService: StorageService,
    private route: ActivatedRoute,
    public translateService: TranslateService
  ) {
    this.route.queryParams.subscribe(params => {
      this.user = JSON.parse(params['user']);
    });
    this.loadTranslations();
  }

  ngOnInit() {}

  goToPage(path) {
    if (!this.user && !this.fAuth.auth.currentUser) {
      this.presentInfo('userData');
      return;
    }

    const navigationExtras: NavigationExtras = {
      queryParams: {
        backPath: 'settings',
        user: JSON.stringify(this.user)
      }
    };
    this.navCtrl.navigateForward([path], navigationExtras);
  }

  removeProfile() {
    this.profileService.removeUser(this.user.uid, this.removeUserAuth);
  }

  removeUserAuth() {
    this.fAuth.auth.currentUser
      .delete()
      .then(() => {
        console.log('User successfully deleted');
        this.logOut();
      })
      .catch(error => {
        console.log(`Error removing user from auth: ${error}`);
      });
  }

  logOut() {
    this.fAuth.auth.signOut();
  }

  getUserName() {
    if (!this.user) {
      return 'User';
    }
    return this.user.name || this.user.email;
  }

  async presentInfo(action: string) {
    let alertParams = {};

    if (action === 'help') {
      alertParams = {
        header: 'Info',
        message: this.translations.info,
        buttons: ['Ok']
      };
    } else if (action === 'remove') {
      alertParams = {
        header: this.translations.warning,
        message: this.translations.delete_confirmation,
        buttons: [
          {
            text: this.translations.cancel,
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: this.translations.delete_account,
            handler: () => {
              this.removeProfile();
            }
          }
        ]
      };
    } else if (action === 'userData') {
      alertParams = {
        header: 'Info',
        message: this.translations.delete_error,
        buttons: ['Ok']
      };
    }

    const alert = await this.alertController.create(alertParams);
    await alert.present();
  }

  loadTranslations() {
    this.translateService.get('SETTINGS.info').subscribe(value => {
      this.translations.info = value;
    });
    this.translateService.get('SETTINGS.warning').subscribe(value => {
      this.translations.warning = value;
    });
    this.translateService.get('SETTINGS.delete_confirmation').subscribe(value => {
      this.translations.delete_confirmation = value;
    });
    this.translateService.get('SETTINGS.cancel').subscribe(value => {
      this.translations.cancel = value;
    });
    this.translateService.get('SETTINGS.delete_account').subscribe(value => {
      this.translations.delete_account = value;
    });
    this.translateService.get('SETTINGS.delete_error').subscribe(value => {
      this.translations.delete_error = value;
    });
  }
}
