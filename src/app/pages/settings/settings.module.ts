import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SettingsPage } from './settings.page';
import { AppHeaderModule } from 'src/app/app-header/app-header.module';
import { MenuModule } from 'src/app/menu/menu.module';
import { SettingsHeaderModule } from 'src/app/settings-header/settings-header.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: SettingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AppHeaderModule,
    MenuModule,
    SettingsHeaderModule,
    TranslateModule
  ],
  declarations: [SettingsPage]
})
export class SettingsPageModule {}
