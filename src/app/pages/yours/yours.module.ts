import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { YoursPage } from './yours.page';
import { AppHeaderModule } from 'src/app/app-header/app-header.module';
import { MenuModule } from 'src/app/menu/menu.module';
import { CardModule } from 'src/app/card/card.module';

const routes: Routes = [
  {
    path: '',
    component: YoursPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AppHeaderModule,
    MenuModule,
    CardModule
  ],
  declarations: [YoursPage]
})
export class YoursPageModule {}
