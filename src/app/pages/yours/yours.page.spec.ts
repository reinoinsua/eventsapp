import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoursPage } from './yours.page';

describe('YoursPage', () => {
  let component: YoursPage;
  let fixture: ComponentFixture<YoursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoursPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
