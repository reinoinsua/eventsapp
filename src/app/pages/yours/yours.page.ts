import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-yours',
  templateUrl: './yours.page.html',
  styleUrls: ['./yours.page.scss']
})
export class YoursPage implements OnInit {
  eventsData: any = [];
  eventsTitle: string;

  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    public fireStore: AngularFirestore,
    public translateService: TranslateService
  ) {
    this.getEvents().subscribe(data => {
      this.eventsData = data;
      if (data.length === 0) {
        this.translateService.get('YOURS.no_events_title').subscribe(value => {
          this.eventsTitle = value;
        });
      } else {
        this.translateService.get('YOURS.own_events').subscribe(value => {
          this.eventsTitle = data.length + ' ' + value;
        });
      }
    });
  }

  ngOnInit() {}

  getEvents() {
    return this.fireStore
      .collection('events', ref =>
        ref.where(
          'createdBy',
          '==',
          this.fAuth.auth.currentUser.email
        )
      )
      .valueChanges();
  }

  goToNewPage() {
    this.navCtrl.navigateForward(['new']);
  }
}
