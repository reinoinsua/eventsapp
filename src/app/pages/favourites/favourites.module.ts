import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FavouritesPage } from './favourites.page';
import { AppHeaderModule } from 'src/app/app-header/app-header.module';
import { MenuModule } from 'src/app/menu/menu.module';
import { CardModule } from 'src/app/card/card.module';

const routes: Routes = [
  {
    path: '',
    component: FavouritesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AppHeaderModule,
    MenuModule,
    CardModule
  ],
  declarations: [FavouritesPage]
})
export class FavouritesPageModule {}
