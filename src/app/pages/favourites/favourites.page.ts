import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.page.html',
  styleUrls: ['./favourites.page.scss']
})
export class FavouritesPage implements OnInit {
  eventsData: any = [];
  eventsTitle: string;

  constructor(
    public fireStore: AngularFirestore,
    public fAuth: AngularFireAuth,
    public navCtrl: NavController,
    public translateService: TranslateService
  ) {
    this.getEvents().subscribe(data => {
      this.eventsData = data;
      if (data.length === 0) {
        this.translateService.get('FAVOURITES.no_events_title').subscribe(value => {
          this.eventsTitle = value;
        });
        this.eventsTitle = `En este momento no hay eventos favoritos`;
      } else {
        this.translateService.get('FAVOURITES.favourite_events').subscribe(value => {
          this.eventsTitle = data.length + ' ' + value;
        });
      }
    });
  }

  ngOnInit() {}

  getEvents() {
    return this.fireStore
              .collection('events',
                ref => ref.where('favouriteBy', 'array-contains', this.fAuth.auth.currentUser.email)).valueChanges();
  }

  goToNewPage() {
    this.navCtrl.navigateForward(['new']);
  }
}
