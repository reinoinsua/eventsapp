import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ActionSheetController, ModalController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ModalPage } from '../../modal/modal.page';
import { Event } from '../../models/event';
import { AngularFirestore } from 'angularfire2/firestore';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-new',
  templateUrl: './new.page.html',
  styleUrls: ['./new.page.scss']
})
export class NewPage implements OnInit {
  eventForm: FormGroup;
  showFirebaseError = false;
  public types: any[];
  public categories: any[];
  public selectedCategories: any[];
  typeSelected = false;
  hideAddressForm = false;
  showSpinner = false;
  showAddressResult = false;
  currentLocation = {};
  imageResult = false;
  base64Image: string;
  newEvent = new Event({});
  translations = {
    create_success: '',
    location_success: '',
    location_error: '',
    add_picture: '',
    picture_file: '',
    make_photo: '',
    cancel: '',
    picture_success: ''
  };


  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    public formBuilder: FormBuilder,
    private geolocation: Geolocation,
    public alertController: AlertController,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    public modalController: ModalController,
    public toastController: ToastController,
    private fireStore: AngularFirestore,
    public translateService: TranslateService
  ) {
    this.loadTypes();
    this.loadCategories();
    this.createForm();
    this.loadTranslations();
  }

  createForm() {
    const numberPattern = '^[0-9]*$';
    this.eventForm = this.formBuilder.group({
      type: ['', Validators.required],
      category: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      contact: ['', Validators.required],
      zipCode: ['', Validators.pattern(numberPattern)],
      avenue: [''],
      street: [''],
      number: ['', Validators.pattern(numberPattern)],
      picture: [''],
      lat: [''],
      long: ['']
    });
  }

  ngOnInit() {}

  createEvent() {
    if (!this.eventForm.valid) {
      this.eventForm.markAllAsTouched();
      return;
    }

    const data = this.eventData();

    this.fireStore.collection('events')
      .doc(data['id'])
      .set(data)
      .then(() => {
        console.log('Event successfully created!');
        this.eventForm.reset();
        this.presentToast(this.translations.create_success);
      })
      .catch((error) => {
        console.log('Error creating event', error);
      });
  }

  eventData() {
    const values = this.eventForm.value;
    const createdAtValue = new Date().getTime();

    const data = {
      id: `${this.fAuth.auth.currentUser.uid}_${createdAtValue}`,
      type: values.type,
      category: values.category,
      title: values.title,
      description: values.description,
      contact: values.contact,
      createdAt: createdAtValue,
      createdBy: this.fAuth.auth.currentUser.email
    };

    if (this.currentLocation['lat'] && this.currentLocation['long']) {
      data['location'] = this.currentLocation;
    }

    if (values.zipCode) {
      data['address'] = {
        zipCode: values.zipCode,
        avenue: values.avenue,
        street: values.street,
        number: values.number,
      };
    }

    if (this.base64Image) {
      data['picture'] = this.base64Image;
    }
    return data;
  }

  loadTypes() {
    this.types = this.newEvent.getTypes();
  }

  loadCategories() {
    this.categories = this.newEvent.getCategories();
  }

  setTypeValues() {
    const type = this.eventForm.value.type;
    this.selectedCategories = this.categories.filter(
      category => category.type === type
    );
    this.typeSelected = true;
  }

  hasError(field: string, error: string) {
    return (
      this.eventForm.get(field).hasError(error) &&
      this.eventForm.get(field).touched
    );
  }

  saveLocation() {
    this.showSpinner = true;
    this.hideAddressForm = true;

    this.geolocation.getCurrentPosition().then((resp) => {
      this.currentLocation['lat'] = resp.coords.latitude;
      this.currentLocation['long'] = resp.coords.longitude;
      this.showSpinner = false;
      this.showAddressResult = true;

      this.resetAddressForm();
      this.presentToast(this.translations.location_success);

    }).catch((error) => {
       console.log('Error getting location', error);
       this.hideAddressForm = false;
       this.showSpinner = false;
       this.showAddressResult = false;
       this.presentInfo(this.translations.location_error);
    });
  }

  resetAddressForm() {
    ['zipCode', 'avenue', 'street', 'number'].forEach((control) => {
      this.eventForm.controls[control].reset();
    });
  }

  showLocationForm() {
    this.hideAddressForm = false;
    this.showAddressResult = false;
  }

  async uploadPicture() {
    const actionSheet = await this.actionSheetController.create({
      header: this.translations.add_picture,
      buttons: [
      {
        text: this.translations.picture_file,
        handler: () => {
          this.takePhoto(0);
          console.log('Share clicked');
        }
      }, {
        text: this.translations.make_photo,
        handler: () => {
          this.takePhoto(1);
        }
      }, {
        text: this.translations.cancel,
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });

    await actionSheet.present();
  }

  takePhoto(sourceType: number) {
    const options: CameraOptions = {
      quality: 20,
      targetHeight: 400,
      targetWidth: 400,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    this.camera.getPicture(options).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,' + imageData;

      this.imageResult = true;

      this.presentToast(this.translations.picture_success);
    }, (error) => {
      console.log(`Error taking the photo: ${error}`);
    });
  }

  async presentImageModal() {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        image: this.base64Image
      }
    });

    modal.onWillDismiss().then((data) => {
      if (data['data']['update']) { this.uploadPicture(); }
      if (data['data']['delete']) {
        this.base64Image = null;
        this.imageResult = false;
      }
    });

    return await modal.present();
  }

  async presentInfo(msg) {
    const alert = await this.alertController.create({
      header: 'Info',
      message: msg,
      buttons: ['Ok']
    });

    await alert.present();
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  loadTranslations() {
    this.translateService.get('NEW.create_success').subscribe(value => {
      this.translations.create_success = value;
    });
    this.translateService.get('NEW.location_success').subscribe(value => {
        this.translations.location_success = value;
      });
    this.translateService.get('NEW.location_error').subscribe(value => {
      this.translations.location_error = value;
    });
    this.translateService.get('NEW.add_picture').subscribe(value => {
      this.translations.add_picture = value;
    });
    this.translateService.get('NEW.picture_file').subscribe(value => {
      this.translations.picture_file = value;
    });
    this.translateService.get('NEW.make_photo').subscribe(value => {
      this.translations.make_photo = value;
    });
    this.translateService.get('NEW.cancel').subscribe(value => {
        this.translations.cancel = value;
    });
    this.translateService.get('NEW.picture_success').subscribe(value => {
      this.translations.picture_success = value;
    });
  }
}
