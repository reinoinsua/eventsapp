import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewPage } from './new.page';
import { AppHeaderModule } from 'src/app/app-header/app-header.module';
import { MenuModule } from 'src/app/menu/menu.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: NewPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AppHeaderModule,
    MenuModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [NewPage]
})
export class NewPageModule {}
