import { Component } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Events, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  eventsData: any = [];
  eventsTitle: string;

  constructor(
    public fireStore: AngularFirestore,
    public fAuth: AngularFireAuth,
    public events: Events,
    public navCtrl: NavController,
    public translateService: TranslateService
  ) {
    this.getEvents().subscribe(data => {
      this.eventsData = data.filter(event => event['createdBy'] !== this.fAuth.auth.currentUser.email);

      if (data.length === 0) {
        this.translateService
              .get('TAB2.no_events_title')
              .subscribe(value => {
                this.eventsTitle = value;
              });
      } else {
        this.translateService
          .get('TAB2.available_events')
          .subscribe(value => {
            this.eventsTitle = this.eventsData.length + ' ' + value;
          });
      }
    });
  }

  getEvents() {
    return this.fireStore.collection('events').valueChanges();
  }

  goToNewPage() {
    this.navCtrl.navigateForward(['new']);
  }
}
