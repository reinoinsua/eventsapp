import { Component, ElementRef } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { NavController, ModalController } from '@ionic/angular';
import { Map, tileLayer, icon } from 'leaflet';
import * as L from 'leaflet';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AngularFirestore } from 'angularfire2/firestore';
import { EventDescriptionPage } from '../event-description/event-description.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  map: Map;
  mapMarkers: any[] = [];
  eventsData: any[];
  currentPosition = {};
  currentPositionIcon = icon({
    iconUrl: 'assets/img/currentPosition_marker.svg',
    iconSize: [24, 24]
  });

  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    public geolocation: Geolocation,
    private fireStore: AngularFirestore,
    private elementRef: ElementRef,
    public modalController: ModalController
  ) {
    this.getEvents().subscribe(data => {
      this.eventsData = data.filter(
        event => event['createdBy'] !== this.fAuth.auth.currentUser.email
      );
      this.drawEvents(this.eventsData);
    });
  }

  ionViewDidEnter() {
    this.leafletMap();
  }

  leafletMap() {
    this.getCurrentLocation();

    this.map = new Map('map').setView([42.878212, -8.544844], 18);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }

  getCurrentLocation() {
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        this.currentPosition = resp;
        this.centerMap(this.currentPosition['coords']);
      })
      .catch(error => {
        console.log('Error getting locations', error);
      });
  }

  centerMap(coords) {
    this.map.panTo(new L.LatLng(coords.latitude, coords.longitude));
    this.drawMarker([coords.latitude, coords.longitude], this.currentPositionIcon, null);
  }

  drawEvents(data) {
    this.removeAllMarkers();

    data.forEach((event) => {
      if (event.location) {
        this.drawMarker([event.location.lat, event.location.long], this.getMarkerOptions(event), event);
      }
    });
  }

  drawMarker(latLong, icon, data) {
    let marker = L.marker(latLong, { icon: icon }).addTo(this.map);
    let popupContent = `<b style="max-width: 100%; white-space: nowrap; overflow: hidden; display: block; text-overflow: ellipsis;">
                          ${data.title}
                        </b>
                        <span style="display: block; text-align: center; margin-top: 3px; color: blue;" class="event-info" data-event='${JSON.stringify(data)}'>+ INFO</span>`;
    marker.bindPopup(popupContent);

    this.map.on('popupopen', (e) => {
      this.elementRef.nativeElement
        .querySelector('.event-info')
        .removeAllListeners();
      this.elementRef.nativeElement.querySelector('.event-info')
        .addEventListener('click', e => {
          const event = e.target.getAttribute('data-event');
          this.openEventInfo(event);
        });
    });
    this.mapMarkers.push(marker);
  }

  removeAllMarkers() {
    if (this.mapMarkers.length > 0) {
      this.mapMarkers.forEach(marker => {
        this.map.removeLayer(marker);
      });
    }
  }

  async openEventInfo(data) {
    if (!data) { return; }
    const modal = await this.modalController.create({
      component: EventDescriptionPage,
      componentProps: {
        event: JSON.parse(data),
        showFavouriteIcon: true
      }
    });

    return await modal.present();
  }

  getMarkerOptions(event) {
    let iconUrl: string;

    if (event.favouriteBy.includes(this.fAuth.auth.currentUser.email)) {
      iconUrl = 'assets/img/favourite_marker.svg';
    } else {
      iconUrl = 'assets/img/marker.svg';
    }

    return icon({
      iconUrl: iconUrl,
      iconSize: [46, 46]
    });
  }

  ionViewWillLeave() {
    this.map.remove();
  }

  getEvents() {
    return this.fireStore.collection('events').valueChanges();
  }
}
