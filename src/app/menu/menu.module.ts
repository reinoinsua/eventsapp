import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [IonicModule, CommonModule, TranslateModule],
  declarations: [MenuComponent],
  exports: [MenuComponent]
})
export class MenuModule {}
