import { Component, OnInit } from '@angular/core';
import { Events, NavController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { ProfileService } from '../services/profile.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { StorageService } from '../services/storage.service';
import { User } from '../models/user';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  menuTitle: String;
  user: User;
  pages = [
    { title: 'MENU.events', path: 'tabs' },
    { title: 'MENU.favourites', path: 'favourites' },
    { title: 'MENU.yours', path: 'yours' },
    { title: 'MENU.new', path: 'new' },
    { title: 'MENU.settings', path: 'settings' }
  ];

  constructor(
    private router: Router,
    public profileService: ProfileService,
    public fAuth: AngularFireAuth,
    public storageService: StorageService,
    public events: Events,
    public navCtrl: NavController,
  ) {
    this.events.subscribe('user:saved', user => {
      this.user = new User(user);
      this.menuTitle = this.getMenuTitle(this.user);
    });
  }

  ngOnInit() {}

  openPage(page) {
    const navigationExtras: NavigationExtras = {
      queryParams: { user: JSON.stringify(this.user) }
    };

    this.navCtrl.navigateForward([page], navigationExtras);
  }

  isActive(page) {
    return this.router.url.split('/')[1] === page.path;
  }

  getMenuTitle(user) {
    const userEmail =
      user && user['email']
        ? user['email'].substring(0, user['email'].lastIndexOf('@'))
        : null;
    return user['name'] || userEmail || 'Menu';
  }
}
