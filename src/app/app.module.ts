import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FirebaseErrorsService } from './services/firebaseErrors.service';
import { LanguageService } from './services/language.service';
import { ProfileService } from './services/profile.service';
import { StorageService } from './services/storage.service';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MenuModule } from './menu/menu.module';
import { IonicStorageModule } from '@ionic/storage';
import { SettingsHeaderModule } from './settings-header/settings-header.module';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { ModalPage } from './modal/modal.page';
import { AppHeaderModule } from './app-header/app-header.module';
import { CardModule } from './card/card.module';
import { EventDescriptionPage } from './event-description/event-description.page';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, ModalPage, EventDescriptionPage],
  entryComponents: [ModalPage, EventDescriptionPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig.fire),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    MenuModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    IonicStorageModule.forRoot(),
    SettingsHeaderModule,
    AppHeaderModule,
    CardModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AngularFireAuth,
    FirebaseErrorsService,
    LanguageService,
    ProfileService,
    StorageService,
    Geolocation,
    Camera
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
