import { Component } from '@angular/core';
import { Events } from '@ionic/angular';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

import { timer } from 'rxjs';

import { LanguageService } from './services/language.service';
import { ProfileService } from './services/profile.service';
import { StorageService} from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  showSplash = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public fAuth: AngularFireAuth,
    private langService: LanguageService,
    public profileService: ProfileService,
    public storageService: StorageService,
    public events: Events
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      this.setRootPage();

      this.langService.setInitialAppLanguage();

      this.hideSplash();
    });
  }

  hideSplash() {
    this.splashScreen.hide();
    timer(3000).subscribe(() => (this.showSplash = false));
  }

  setRootPage() {
    this.fAuth.authState.subscribe((user: firebase.User) => {
      const rootPage = user && user.emailVerified ? 'tabs' : 'login';
      if (user) { this.profileService.getUser(user.uid); }
      this.router.navigate([rootPage]);
    });
  }
}
