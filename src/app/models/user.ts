export class User {
  email: string;
  name: string;
  locale: string;
  pictureUrl: string;
  uid: string;


  constructor(params) {
    Object.assign(this, params);
  }

  getUid() {
    return this.uid;
  }
}
