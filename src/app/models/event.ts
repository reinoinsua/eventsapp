export class Event {
  id: string;
  type: string;
  category: string;
  title: string;
  description: string;
  contact: string;
  location: {
    lat: string,
    long: string
  };
  address: {
    zipCode: number,
    avenue: string,
    street: string,
    number: number
  };
  picture: string;
  createdAt: number;
  createdBy: string;
  favouriteBy: string[];

  constructor(params) {
    Object.assign(this, params);
  }

  getTypes() {
    return [
      { value: 'sales', name: 'TYPES.sales' },
      { value: 'event', name: 'TYPES.event' }
    ];
  }

  getCategories() {
    return [
      { type: 'event', value: 'public', name: 'CATEGORIES.public' },
      { type: 'event', value: 'private', name: 'CATEGORIES.private' },
      { type: 'sales', value: 'mode', name: 'CATEGORIES.mode' },
      {
        type: 'sales',
        value: 'health_and_beauty',
        name: 'CATEGORIES.health_and_beauty'
      },
      { type: 'sales', value: 'home', name: 'CATEGORIES.home' },
      { type: 'sales', value: 'technology', name: 'CATEGORIES.technology' },
      { type: 'sales', value: 'motor', name: 'CATEGORIES.motor' },
      { type: 'sales', value: 'restaurants', name: 'CATEGORIES.restaurants' },
      {
        type: 'sales',
        value: 'insurance_and_finances',
        name: 'CATEGORIES.insurance_and_finances'
      },
      { type: 'sales', value: 'sports', name: 'CATEGORIES.sports' },
      { type: 'sales', value: 'free_time', name: 'CATEGORIES.free_time' },
      { type: 'sales', value: 'kids', name: 'CATEGORIES.kids' },
      { type: 'sales', value: 'gifts', name: 'CATEGORIES.gifts' }
    ];
  }
}
