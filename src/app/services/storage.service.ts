import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageService {
  constructor(private storage: Storage) {}

  setDataInStorage(key, data): any {
    this.storage.set(key, JSON.stringify(data));
  }

  getDataFromStorage(key) {
    let data = {};
    try {
      this.storage.get(key).then((dat) => {
        console.log(`Data: ${dat}`);
        data = JSON.parse(dat);
      });
    } catch(error) {
      console.log(`Error loading ${key} data: ${error}`);
    }

    if (data) { return data; }
  }
}
