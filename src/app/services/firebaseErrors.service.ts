import { Injectable } from '@angular/core';

@Injectable()
export class FirebaseErrorsService {

  constructor() {}

  renderErrorMsg(code: string) {
    if (!code) { return 'Error'; }

    let message = '';

    switch (code) {
      case 'auth/user-not-found': {
        message = 'Not found';
        break;
      }
      case 'auth/email-already-in-use': {
        message = 'The email address is already in use by another account.';
        break;
      }
      case 'auth/weak-password': {
        message = 'Password should be at least 6 characters';
        break;
      }
      case 'auth/wrong-password': {
        message = 'Wrong password';
        break;
      }
      default: {
        message = 'Error';
        console.log('No firebase code', code);
        break;
      }
    }

    return message;
  }
}
