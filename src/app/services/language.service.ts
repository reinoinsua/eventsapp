import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable()
export class LanguageService {
  selected = '';

  constructor(
    private translate: TranslateService,
  ) {}

  setInitialAppLanguage() {
    let language = this.translate.currentLang || this.translate.getBrowserLang() || 'es';
    this.translate.setDefaultLang(language);
  }

  setLanguage(lng) {
    this.translate.use(lng);
    this.selected = lng;
  }
}
