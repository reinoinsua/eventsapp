import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Events } from '@ionic/angular';
import { StorageService } from './storage.service';
import { LanguageService } from './language.service';

@Injectable()
export class ProfileService {
  constructor(
    private fireStore: AngularFirestore,
    public fAuth: AngularFireAuth,
    public events: Events,
    public storageService: StorageService,
    public langService: LanguageService
  ) {}

  getUser(userId) {
    let data;
    const docRef = this.fireStore.collection('profiles').doc(userId);

    docRef.ref.get().then(doc => {
      if (doc.exists) {
        data = doc.data();
        this.storageService.setDataInStorage('USER', data);
        this.events.publish('user:saved', data);
        this.langService.setLanguage(data.locale);
      }
    });

    return data;
  }

  removeUser(userId, callback) {
    this.fireStore
      .collection('profiles')
      .doc(userId)
      .delete()
      .then(() => {
        console.log('Profile successfully deleted!');
        callback();
      })
      .catch(error => {
        console.error(`Error removing profile: ${error}`);
      });
  }

  createUser(userId, data) {
    this.fireStore
      .doc(`/profiles/${userId}`)
      .set(data)
      .then((doc) => {
        console.log('Profile successfully created!');
        this.getUser(userId);
      })
      .catch((error) => {
        console.log(`Error creating profile: ${error}`);
      });
  }

  updateUser(userId, data) {
    this.fireStore
      .collection('profiles')
      .doc(userId)
      .update(data)
      .then(() => {
        console.log('Profile successfully updated!');
        this.getUser(userId);
      })
      .catch(error => {
        if (error.code === 'not-found') {
          this.createUser(userId, data);
        }
        console.log(`Error updating profile: ${error}`);
      });
  }
}
