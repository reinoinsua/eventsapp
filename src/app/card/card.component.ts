import { Component, OnInit, Input } from '@angular/core';
import { Event } from '../models/event';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController, ToastController, ModalController, NavController } from '@ionic/angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { EventDescriptionPage } from '../event-description/event-description.page';
import { NavigationExtras } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() eventInput: Event;
  icons = [
    { icon: 'trash', ownEvent: true },
    { icon: 'heart', ownEvent: false },
    { icon: 'create', ownEvent: true }
  ];

  translations = {
    cancel: '',
    warning: '',
    delete_confirmation: '',
    delete: ''
  };

  constructor(
    public fAuth: AngularFireAuth,
    public alertController: AlertController,
    public fireStore: AngularFirestore,
    public toastController: ToastController,
    public modalController: ModalController,
    public navCtrl: NavController,
    public translateService: TranslateService
  ) {}

  ngOnInit() {}

  showIcon(iconInput, createdBy) {
    const currentUserEmail = this.fAuth.auth.currentUser.email;
    let result;

    this.icons.forEach(icon => {
      if (icon.icon === iconInput) {
        if (icon.ownEvent) {
          result = currentUserEmail === createdBy;
        }
        if (!icon.ownEvent) {
          result = currentUserEmail !== createdBy;
        }
      }
    });
    return result;
  }

  isFavourite(event) {
    if (!event.favouriteBy || event.favouriteBy.length === 0) {
      return false;
    }
    return event.favouriteBy.includes(this.fAuth.auth.currentUser.email);
  }

  addToFavourites(event) {
    const currentUser = this.fAuth.auth.currentUser.email;
    const data = event.favouriteBy ? event.favouriteBy : [];
    let updatedData = [];

    if (data) {
      if (data.includes(currentUser)) {
        const index = data.indexOf(currentUser);
        data.splice(index, 1);
        updatedData = data;
      } else {
        data.push(currentUser);
        updatedData = data;
      }
    }

    this.fireStore
      .collection('events')
      .doc(event.id)
      .update({
        favouriteBy: updatedData
      })
      .then(() => {
        console.log('Event successfully updated!');
      })
      .catch((error) => {
        console.log('Error updating event:', error);
      });
  }

  async removeEventConfirmation(event) {
    this.getTranslations();

    const params = {
      header: this.translations.warning,
      message: this.translations.delete_confirmation,
      buttons: [
        {
          text: this.translations.cancel,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.translations.delete,
          handler: () => {
            this.removeEvent(event);
          }
        }
      ]
    };

    const alert = await this.alertController.create(params);
    await alert.present();
  }

  getTranslations() {
    this.translateService.get('CARD.warning').subscribe(value => {
      this.translations.warning = value;
    });
    this.translateService.get('CARD.delete_confirmation').subscribe(value => {
      this.translations.delete_confirmation = value;
    });
    this.translateService.get('CARD.cancel').subscribe(value => {
      this.translations.cancel = value;
    });
    this.translateService.get('CARD.delete').subscribe(value => {
      this.translations.delete = value;
    });
  }

  removeEvent(event) {
    this.fireStore
      .collection('events')
      .doc(event.id)
      .delete()
      .then(() => {
        console.log('Event successfully deleted!');
        this.translateService
          .get('CARD.delete_success')
          .subscribe(value => {
            this.presentToast(value);
          });
      })
      .catch(error => {
        console.error('Error removing event: ', error);
      });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async showEvent(event) {
    const modal = await this.modalController.create({
      component: EventDescriptionPage,
      componentProps: {
        event: event
      }
    });

    return await modal.present();
  }

  updateEvent(event) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        event: JSON.stringify(event)
      }
    };

    this.navCtrl.navigateForward(['editEvent'], navigationExtras);
  }
}
