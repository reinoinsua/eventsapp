import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {
  @Input() public showMenuButton = 1;
  @Input() public backButton = 1;
  @Input() public backButtonPath: string;

  constructor() {}

  ngOnInit() {}
}
