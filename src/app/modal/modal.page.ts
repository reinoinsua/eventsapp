import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'modal-page',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage {
  image: string;
  modalData = {
    update: false,
    delete: false
  };

  constructor(
    public navCtrl: NavController,
    public modalController: ModalController,
    public navParams: NavParams
  ) {
    this.image = navParams.get('image');
  }

  closeModal(data) {
    this.modalController.dismiss(data);
  }

  updatePicture() {
    this.modalData['update'] = true;
    this.closeModal(this.modalData);
  }

  deletePicture() {
    this.modalData['delete'] = true;
    this.closeModal(this.modalData);
  }
}
