import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseErrorsService } from '../services/firebaseErrors.service';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss']
})
export class ResetPasswordPage implements OnInit {
  resetForm: FormGroup;
  showFirebaseError = false;
  firebaseMessage: string;
  backPath = 'login';
  msg: string;

  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    private formBuilder: FormBuilder,
    public alertController: AlertController,
    private fErrorsService: FirebaseErrorsService,
    private route: ActivatedRoute,
    public translateService: TranslateService
  ) {
    this.route.queryParams.subscribe(params => {
      this.backPath = params['backPath'];
    });

    this.createForm();
  }

  ngOnInit() {}

  createForm() {
    this.resetForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  resetPassword() {
    if (!this.resetForm.valid) {
      this.resetForm.markAllAsTouched();
      return;
    }

    this.fAuth.auth
      .sendPasswordResetEmail(this.resetForm.value.email)
      .then(() => {
        this.navCtrl.navigateRoot(['login']);
        this.presentAlert();
      })
      .catch(error => {
        this.firebaseMessage = this.fErrorsService.renderErrorMsg(error.code);
        this.showFirebaseError = true;
      });
  }

  hasError(field: string, error: string) {
    return (
      this.resetForm.get(field).hasError(error) &&
      this.resetForm.get(field).touched
    );
  }

  async presentAlert() {
    this.translateService
      .get('RESET_PASSWORD.info')
      .subscribe(value => {
        this.msg = value;
      });

    const alert = await this.alertController.create({
      header: 'Info',
      message: 'Email sent',
      buttons: ['Ok']
    });

    await alert.present();
  }
}
