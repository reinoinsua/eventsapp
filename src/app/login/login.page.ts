import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FirebaseErrorsService } from '../services/firebaseErrors.service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  showFirebaseError = false;
  firebaseMessage: string;

  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    private formBuilder: FormBuilder,
    public alertController: AlertController,
    public fErrorsService: FirebaseErrorsService
  ) {
    this.createForm();
  }

  ngOnInit() {}

  createForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required]
    });
  }

  login() {
    if (!this.loginForm.valid) {
      this.loginForm.markAllAsTouched();
      return;
    }

    this.fAuth.auth
      .signInWithEmailAndPassword(
        this.loginForm.value.email,
        this.loginForm.value.password
      )
      .then(user => {
        this.showFirebaseError = false;

        if (user.user.emailVerified) {
          console.log('Successfully logged in!');

          this.navCtrl.navigateRoot(['tabs']);
        } else {
          console.log('User without email verified');
          this.presentAlert();
        }
      })
      .catch(error => {
          this.firebaseMessage = this.fErrorsService.renderErrorMsg(error.code);
          this.showFirebaseError = true;
      });
  }

  goToRegisterPage() {
    this.navCtrl.navigateRoot(['register']);
  }

  goToResetPassPage() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        backPath: 'login',
      }
    };
    this.navCtrl.navigateForward(['resetPassword'], navigationExtras);
  }

  hasError(field: string, error: string) {
    return (
      this.loginForm.get(field).hasError(error) &&
      this.loginForm.get(field).touched
    );
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Info',
      message: 'User must be validate the email',
      buttons: ['Ok']
    });

    await alert.present();
  }
}
