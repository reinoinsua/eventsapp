import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, ToastController, ActionSheetController, ModalController, AlertController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { ActivatedRoute } from '@angular/router';
import { Event } from '../models/event';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ModalPage } from '../modal/modal.page';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.page.html',
  styleUrls: ['./edit-event.page.scss']
})
export class EditEventPage implements OnInit {
  editEventForm: FormGroup;
  public types: any[];
  event: Event;
  public categories: any[];
  public selectedCategories: any[];
  typeSelected = false;
  showSpinner = false;
  hideAddressForm = false;
  showAddressResult = false;
  currentLocation = {};
  imageResult = false;
  base64Image: string;
  translations = {
    update_success: '',
    location_success: '',
    location_error: '',
    add_picture: '',
    picture_file: '',
    make_photo: '',
    cancel: '',
    picture_success: ''
  };

  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    public formBuilder: FormBuilder,
    public toastController: ToastController,
    private fireStore: AngularFirestore,
    private route: ActivatedRoute,
    private geolocation: Geolocation,
    private camera: Camera,
    public actionSheetController: ActionSheetController,
    public modalController: ModalController,
    public alertController: AlertController,
    public translateService: TranslateService
  ) {
    this.route.queryParams.subscribe(params => {
      this.event = new Event(JSON.parse(params['event']));

      this.loadTypes();
      this.loadCategories();
      this.createEditForm();
      this.setTypeValues();
      this.loadTranslations();

      if (this.event.location) {
        this.currentLocation = this.event.location;
        this.hideAddressForm = true;
        this.showAddressResult = true;
      }

      if (this.event.picture) {
        this.base64Image = this.event.picture;
        this.imageResult = true;
      }
    });
  }

  ngOnInit() {}

  createEditForm() {
    const numberPattern = '^[0-9]*$';
    this.editEventForm = this.formBuilder.group({
      type: ['', Validators.required],
      category: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      contact: ['', Validators.required],
      zipCode: ['', Validators.pattern(numberPattern)],
      avenue: [''],
      street: [''],
      number: ['', Validators.pattern(numberPattern)],
      picture: [''],
      lat: [''],
      long: ['']
    });

    this.editEventForm.controls['type'].setValue(this.event.type);
    this.editEventForm.controls['category'].setValue(this.event.category);
    this.editEventForm.controls['title'].setValue(this.event.title);
    this.editEventForm.controls['description'].setValue(this.event.description);
    this.editEventForm.controls['contact'].setValue(this.event.contact);

    if (this.event.address) {
      this.editEventForm.controls['zipCode'].setValue(
        this.event.address.zipCode
      );
      this.editEventForm.controls['avenue'].setValue(this.event.address.avenue);
      this.editEventForm.controls['street'].setValue(this.event.address.street);
      this.editEventForm.controls['number'].setValue(this.event.address.number);
    }
  }

  loadTypes() {
    this.types = this.event.getTypes();
  }

  loadCategories() {
    this.categories = this.event.getCategories();
  }

  setTypeValues() {
    const type = this.event.type || this.editEventForm.value.type;

    this.selectedCategories = this.categories.filter(
      category => category.type === type
    );
    this.typeSelected = true;
  }

  updateEvent() {
    if (!this.editEventForm.valid) {
      this.editEventForm.markAllAsTouched();
      return;
    }

    const data = this.eventData();

    this.fireStore
      .collection('events')
      .doc(this.event.id)
      .update(data)
      .then(() => {
        console.log('Event successfully updated!');
        this.presentToast(this.translations.update_success);
      })
      .catch(error => {
        console.log('Error updating event', error);
      });
  }

  hasError(field: string, error: string) {
    return (
      this.editEventForm.get(field).hasError(error) &&
      this.editEventForm.get(field).touched
    );
  }

  eventData() {
    const values = this.editEventForm.value;

    const data = {
      type: values.type,
      category: values.category,
      title: values.title,
      description: values.description,
      contact: values.contact
    };

    if (this.currentLocation['lat'] && this.currentLocation['long']) {
      data['location'] = this.currentLocation;
    }

    if (values.zipCode) {
      data['address'] = {
        zipCode: values.zipCode,
        avenue: values.avenue,
        street: values.street,
        number: values.number
      };
    }

    if (this.base64Image) {
      data['picture'] = this.base64Image;
    }
    return data;
  }

  saveLocation() {
    this.showSpinner = true;
    this.hideAddressForm = true;

    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        this.currentLocation['lat'] = resp.coords.latitude;
        this.currentLocation['long'] = resp.coords.longitude;
        this.showSpinner = false;
        this.showAddressResult = true;

        this.resetAddressForm();
        this.presentToast(this.translations.location_success);
      })
      .catch(error => {
        console.log('Error getting location', error);
        this.hideAddressForm = false;
        this.showSpinner = false;
        this.showAddressResult = false;
        this.presentInfo(this.translations.location_error);
      });
  }

  resetAddressForm() {
    ['zipCode', 'avenue', 'street', 'number'].forEach(control => {
      this.editEventForm.controls[control].reset();
    });
  }

  showLocationForm() {
    this.hideAddressForm = false;
    this.showAddressResult = false;
  }

  async uploadPicture() {
    const actionSheet = await this.actionSheetController.create({
      header: this.translations.add_picture,
      buttons: [
        {
          text: this.translations.picture_file,
          handler: () => {
            this.takePhoto(0);
            console.log('Share clicked');
          }
        },
        {
          text: this.translations.make_photo,
          handler: () => {
            this.takePhoto(1);
          }
        },
        {
          text: this.translations.cancel,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    await actionSheet.present();
  }

  takePhoto(sourceType: number) {
    const options: CameraOptions = {
      quality: 20,
      targetHeight: 400,
      targetWidth: 400,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    };

    this.camera.getPicture(options).then(
      imageData => {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;

        this.imageResult = true;

        this.presentToast(this.translations.picture_success);
      },
      error => {
        console.log(`Error taking the photo: ${error}`);
      }
    );
  }

  async presentImageModal() {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        image: this.base64Image
      }
    });

    modal.onWillDismiss().then(data => {
      if (data['data']['update']) {
        this.uploadPicture();
      }
      if (data['data']['delete']) {
        this.base64Image = null;
        this.imageResult = false;
      }
    });

    return await modal.present();
  }

  async presentInfo(msg) {
    const alert = await this.alertController.create({
      header: 'Info',
      message: msg,
      buttons: ['Ok']
    });

    await alert.present();
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  loadTranslations() {
    this.translateService.get('NEW.update_success').subscribe(value => {
      this.translations.update_success = value;
    });
    this.translateService.get('NEW.location_success').subscribe(value => {
      this.translations.location_success = value;
    });
    this.translateService.get('NEW.location_error').subscribe(value => {
      this.translations.location_error = value;
    });
    this.translateService.get('NEW.add_picture').subscribe(value => {
      this.translations.add_picture = value;
    });
    this.translateService.get('NEW.picture_file').subscribe(value => {
      this.translations.picture_file = value;
    });
    this.translateService.get('NEW.make_photo').subscribe(value => {
      this.translations.make_photo = value;
    });
    this.translateService.get('NEW.cancel').subscribe(value => {
      this.translations.cancel = value;
    });
    this.translateService.get('NEW.picture_success').subscribe(value => {
      this.translations.picture_success = value;
    });
  }
}
