import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FirebaseErrorsService } from '../services/firebaseErrors.service';
import { TranslateService } from '@ngx-translate/core';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  showFirebaseError = false;
  firebaseMessage: string;
  msg: string;

  constructor(
    public navCtrl: NavController,
    public fAuth: AngularFireAuth,
    private formBuilder: FormBuilder,
    public alertController: AlertController,
    public fErrorsService: FirebaseErrorsService,
    private fireStore: AngularFirestore, 
    public translateService: TranslateService
  ) {
    this.createForm();
  }

  ngOnInit() {}

  createForm() {
    this.registerForm = this.formBuilder.group(
      {
        email: [
          '',
          Validators.compose([Validators.required, Validators.email])
        ],
        password: ['', Validators.required],
        passwordConfirmation: ['', Validators.required]
      },
      { validator: this.checkPasswords }
    );
  }

  checkPasswords(group: FormGroup) {
    const password = group.get('password').value;
    const passwordConfirmation = group.get('passwordConfirmation').value;

    return password === passwordConfirmation ? null : { notSame: true };
  }

  async register() {
    if (!this.registerForm.valid) {
      this.registerForm.markAllAsTouched();
      return;
    }

    this.fAuth.auth
      .createUserWithEmailAndPassword(
        this.registerForm.value.email,
        this.registerForm.value.password
      )
      .then(credential => {
        this.sendEmailVerification();
        console.log('Successfully registered!');
        this.navCtrl.navigateRoot(['login']);

        this.fireStore
          .doc(`/profiles/${credential.user.uid}`)
          .set({ email: credential.user.email, uid: credential.user.uid, locale: 'es' });

        this.presentInfo();
      })
      .catch(error => {
        this.firebaseMessage = this.fErrorsService.renderErrorMsg(error.code);
        this.showFirebaseError = true;
      });
  }

  sendEmailVerification() {
    this.fAuth.authState.subscribe(user => {
      user.sendEmailVerification().then(() => {
        console.log('Email sent');
      });
    });
  }

  goToLoginPage() {
    this.navCtrl.navigateRoot(['login']);
  }

  hasError(field: string, error: string) {
    return (
      this.registerForm.get(field).hasError(error) && this.registerForm.get(field).touched
    );
  }

  async presentInfo() {
    this.translateService.get('REGISTER.password_confirmation_info').subscribe(value => {
      this.msg = value;
    });

    const alert = await this.alertController.create({
      header: 'Info',
      message: this.msg,
      buttons: ['Ok']
    });

    await alert.present();
  }
}
