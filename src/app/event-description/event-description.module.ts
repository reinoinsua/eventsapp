import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventDescriptionPage } from './event-description.page';
import { AppHeaderModule } from '../app-header/app-header.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: EventDescriptionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AppHeaderModule,
    TranslateModule
  ],
  declarations: [EventDescriptionPage]
})
export class EventDescriptionPageModule {}
