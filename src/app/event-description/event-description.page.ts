import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, NavParams } from '@ionic/angular';
import { Event } from '../models/event';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-event-description',
  templateUrl: './event-description.page.html',
  styleUrls: ['./event-description.page.scss']
})
export class EventDescriptionPage implements OnInit {
  event: Event;
  showFavouriteIcon = false;

  constructor(
    public navCtrl: NavController,
    public modalController: ModalController,
    public navParams: NavParams,
    public fAuth: AngularFireAuth,
    private fireStore: AngularFirestore
  ) {
    this.event = navParams.get('event');
    if (navParams.get('showFavouriteIcon')) {
      this.showFavouriteIcon = navParams.get('showFavouriteIcon');
    }
  }

  ngOnInit() {}

  closeModal(data) {
    this.modalController.dismiss(data);
  }

  isFavourite(event) {
    if (!event.favouriteBy || event.favouriteBy.length === 0) {
      return false;
    }
    return event.favouriteBy.includes(this.fAuth.auth.currentUser.email);
  }

  addToFavourites(event) {
    const currentUser = this.fAuth.auth.currentUser.email;
    const data = event.favouriteBy ? event.favouriteBy : [];
    let updatedData = [];

    if (data) {
      if (data.includes(currentUser)) {
        const index = data.indexOf(currentUser);
        data.splice(index, 1);
        updatedData = data;
      } else {
        data.push(currentUser);
        updatedData = data;
      }
    }

    this.fireStore
      .collection('events')
      .doc(event.id)
      .update({
        favouriteBy: updatedData
      })
      .then(() => {
        console.log('Event successfully updated!');
      })
      .catch(error => {
        console.log('Error updating event:', error);
      });
  }
}
